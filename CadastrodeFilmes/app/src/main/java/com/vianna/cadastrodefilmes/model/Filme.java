package com.vianna.cadastrodefilmes.model;

import com.orm.SugarRecord;

public class Filme extends SugarRecord {

    private String nomeFilme;
    private String genero;

    public Filme() {
    }

    public Filme(String nomeFilme, String genero) {
        this.nomeFilme = nomeFilme;
        this.genero = genero;
    }

    public String getNomeFilme() {
        return nomeFilme;
    }

    public void setNomeFilme(String nomeFilme) {
        this.nomeFilme = nomeFilme;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
