package com.vianna.cadastrodefilmes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.vianna.cadastrodefilmes.model.Filme;

public class MainActivity extends AppCompatActivity {

    private TextInputLayout edtFilme, edtGenero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtFilme = findViewById(R.id.tilNome);
        edtGenero = findViewById(R.id.tilGenero);
    }

    public void salvar(View v){
        Filme f = new Filme(edtFilme.getEditText().getText().toString(), edtGenero.getEditText().getText().toString());
        f.save();
        Toast.makeText(this, "Filme salvo com sucesso!",Toast.LENGTH_SHORT).show();
    }
}